require('dotenv').config();
console.log(process.env.test);
var express = require('express');
var app = new express();
var cookieParser = require('cookie-parser');
var csurf = require('csurf');

app.set('view engine', 'pug');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.use(cookieParser());
app.use(csurf({cookie: true}));

var userRoute = require('./Routes/user.route');
var authRoute = require('./Routes/auth.route');
app.use('/user', userRoute);
app.use('/auth', authRoute);

app.listen(1002);