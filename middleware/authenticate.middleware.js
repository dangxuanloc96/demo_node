module.exports.checkAuthenticate = (req, res, next) => {
    console.log(req.cookies);
    if (!req.cookies.userID) {
        res.render('Auth/login', {
            message: 'Bạn chưa đăng nhập'
        });
        return;
    }
    next();
};