module.exports.postCreate = (req, res, next) => {
    var errors = [];
    if (!req.body.id) {
        errors.push('ID không được để trống');
    }
    if (!req.body.name) {
        errors.push('Tên không được để trống');
    }
    if(errors.length > 0) {
        res.render('user/create', {
            errors: errors
        });
        return;
    } else {
        next();
    }
};