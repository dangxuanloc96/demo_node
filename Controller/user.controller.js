var db = require('../db');

module.exports.list = (req, res) => {
    let page = req.query.page ? req.query.page : 1;
    let page_size = req.query.page_size ? req.query.page_size : 5;
    let listUser = db.get('user').value();
    let start = page_size * (page - 1);
    let end = page_size * page;
    let resultSearch = listUser.slice(start, end);

    // db.defaults({user: [
    //         {
    //             id: 1,
    //             name: 'Đặng Xuân Lộc'
    //         }
    //     ]})
    //     .write();
    res.render('User/list', {
        list: resultSearch
    });
};

module.exports.search = (req, res) => {
    let search = req.query.search;
    if (search === '') {
        res.redirect('list');
    } else {
        let keySearch = req.query.search;
        let listUser = db.get('user').value();
        let result = listUser.map((value) => {
            if (value.name.indexOf(keySearch) >= 0) {
                return value;
            }
        });
       if (result.length == 1 && result[0] == undefined) {
           res.render('user/list', {
               list: []
           });
       } else {
           console.log(result);
           res.render('user/list', {
               list: result
           });
       }
    }
};

module.exports.create = (req, res) => {
    db.defaults({token : [
        ]})
        .write();
    let token = req.csrfToken();
    db.get('token').push({key : token}).write();
    res.render('user/create', {
        session: {csrf_token : token}
    });
};

module.exports.store = (req, res) => {
    db.get('user').push(req.body).write();
    res.render('user/create', {
        success: "Thêm thành công"
    });
};

module.exports.delete = (req, res) => {
    let idUser = req.params.id;
    console.log(idUser);
    db.get('user')
        .remove({ id : idUser})
        .write();
    res.render('user/list');
};

module.exports.test = (req, res) => {
    console.log(process.env);
};