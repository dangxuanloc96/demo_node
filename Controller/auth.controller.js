var db = require('../db');
module.exports.login = (req, res) => {
   res.render('Auth/login');
};

module.exports.postLogin = (req, res) => {
    let name = req.body.name;
    if (name) {
       let check =  db.get('user').find({name: name}).value();
       if (!check) {
           res.render('Auth/login', {
               'message' : 'Tài khoản không tồn tại'
           });
           return;
       }
       if (check.password+'' !== req.body.password) {
           res.render('Auth/login', {
               'message' : 'Mật khẩu sai'
           });
           return;
       }
           res.cookie('userID', check.id);
           res.redirect('/user/list');
    } else {
        res.render('Auth/login', {
            'message' : 'UserName không được để trống'
        });
    }
};