const express = require('express');
const router = new express.Router();
var authRoute = require('../Controller/auth.controller');

router.get('/login', authRoute.login);
router.post('/postLogin', authRoute.postLogin);
module.exports = router;