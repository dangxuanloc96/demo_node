const express = require('express');
var multer = require('multer');
const router = new express.Router();
var userController = require('../Controller/user.controller');
var validateUser = require('../Validate/userCreateValidate');
var upload = multer({dest: '../public/uploads/'});
var checkToken = require('../middleware/checkToken.middleware');

router.get('/list', userController.list);
router.get('/search', userController.search);
router.get('/create',userController.create);
router.post('/store', checkToken.checkToken, upload.single('avatar'), validateUser.postCreate, userController.store);
router.get('/delete/:id', userController.delete);
router.get('/test', userController.test);

module.exports = router;
